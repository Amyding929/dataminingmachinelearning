package com.example.readwrite;

import java.io.BufferedReader;
import java.io.FileInputStream; 
import java.io.IOException;
import java.io.InputStreamReader; 
import java.math.BigDecimal;

public class CheckCorrect {
	/*定义划分块，使方便能根据块输出samplId tagClass，
	 * 方便自己预测值于实际预测文件中值进行比较
	 * */
	public static int[] blockArr = { 31, 35, 88, 44, 29, 24, 40, 50, 29 };

	/* ae.train的结果通过size_ae.train划分，存储到文件中：sampleId tagClass */
	public static void outputCompareTest(String outputPathStr) {
		// 写到文档中
		ReadWriteTest rw = new ReadWriteTest();
		String outputPath = outputPathStr;
		String outputContent = "";
		int count = 0;// 1-370
		for (int i = 0; i < 9; i++) {
			int len = blockArr[i];
			// block[0]
			for (int j = 0; j < len; j++) {// 出现的遍数0-31是类1
				count++;
				outputContent += count + " " + (i + 1) + "\r\n";
			}
		} 
		rw.writerTxt(outputContent, outputPath);
	}

	/*比较两个文件内容，输出正确预测值的概率*/
	@SuppressWarnings("resource")
	public static double predictionCorrect(String file1, String file2) {
		double estimation = 0.0;
		String filePath1 = file1;
		BufferedReader reader1 = null;
		String filePath2 = file2;
		BufferedReader reader2 = null;
		String str1 = null;
		String str2 = null;
		int correctNum = 0;
		double sumNum = 370.0;
		try {
			reader1 = new BufferedReader(new InputStreamReader(
					new FileInputStream(filePath1), "UTF-8"));
			reader2 = new BufferedReader(new InputStreamReader(
					new FileInputStream(filePath2), "UTF-8"));
			while (((str1 = reader1.readLine()) != null)
					&& ((str2 = reader2.readLine()) != null)) {
				String[] lineArr1 = str1.split(" ");
				String[] lineArr2 = str2.split(" ");
				/**读取两个文件中的值*/
				if ((lineArr1[0].equals(lineArr2[0]))
						&& (lineArr1[1].equals(lineArr2[1]))) {
					correctNum++;
				}
			}
			estimation = correctNum / sumNum * 100;
			BigDecimal bd = new BigDecimal(estimation);

			System.out.println("估计样本量：370");
			System.out.println("估计成功数目:" + correctNum);
			System.out.println("估计成功率:"
					+ String.valueOf(bd.setScale(2, BigDecimal.ROUND_HALF_UP)
							.doubleValue()) + "%");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return estimation;
	}

	public static void main(String[] args) throws Exception {
		long t1 = System.currentTimeMillis();
		System.out.println("--------------------1:调用第一类----------------------");
		ReadWriteTest rwt1 = new ReadWriteTest();
		// flag 表示对test（1）操作还是train（0）操作 ,参数中有flag
		// train
		String filePathTrain = "D://bigdata//ae.train";
		String strTrain = rwt1.readTxt(filePathTrain, 0);
		String outputPathTrain = "D://bigdata//train-9-30.txt";
		rwt1.writerTxt(strTrain, outputPathTrain);
		// test
		String filePathTest = "D://bigdata//ae.test";
		String strTest = rwt1.readTxt(filePathTest, 1);
		String outputPathTest = "D://bigdata//test-270.txt";
		rwt1.writerTxt(strTest, outputPathTest);

		System.out.println("--------------------2:调用第二类----------------------");
		SampleClassify sampleClass2 = new SampleClassify();
		/* 训练集获取类和对应矩阵维度30*13,存储在mapSpeakers中 */
		sampleClass2.readTxtTrain(outputPathTrain);
		/* 读取测试集样本，每个代入计算距离，取最小距离，返回最小距离的类标 */
		String sampleIdTagClassContent = sampleClass2
				.readTxtTest(outputPathTest); 
		/* 把（sampleId tagClass）放到PrepredictionResult.txt文件中 */
		ReadWriteTest rw = new ReadWriteTest();
		String outputPathPredictionResult = "D://bigdata//PredictionResult.txt";
		rw.writerTxt(sampleIdTagClassContent, outputPathPredictionResult);

		System.out.println("--------------------3:调用第三类----------------------");
		/* ae.train的结果通过size_ae.train划分，存储到文件中：sampleId tagClass */
		String outputPathCompareResult = "D://bigdata//CompareResult.txt";
		outputCompareTest(outputPathCompareResult);

		/* 比较两个文件正确值 */
		predictionCorrect(outputPathCompareResult, outputPathPredictionResult);
		System.out.println("\r程序运行时间: " + (System.currentTimeMillis() - t1)
				/ 1000f + " s ");

	}
}
