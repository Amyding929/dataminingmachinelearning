package com.example.readwrite;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException; 
import java.util.HashMap;
import java.util.Map;
/*该类作用是计算并输出每个样本和其所对应的发音者类*/
public class SampleClassify { 
	
	/*存储所有发音者类对应其行（每行是一个块的转换），
	 * key是发音者类，value是对应30个行（用二维数组存储）
	 * */
	public static Map<Integer, double[][]> mapSpeakers = new HashMap<Integer, double[][]>();

	/*初始化矩阵，30*13*/
	public static double[][] getSpeakerMatrix() {
		double[][] speaker = new double[30][13];
		/*初使化矩阵*/
		for (int i = 0; i < 30; i++) {
			for (int j = 0; j < 13; j++) {
				speaker[i][j] = 0;
			}
		}
		return speaker;

	}

	/*读取train文件值，存储到mapSpeakers中*/
	public void readTxtTrain(String fileTrainPath ) {
		String fileTrain = fileTrainPath;
		BufferedReader reader = null;
		double[] arr13 = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		try {
			reader = new BufferedReader(new InputStreamReader(
			/* 指定读取文件的编码格式，要和写入的格式一致，以免出现中文乱码*/
					new FileInputStream(fileTrain), "UTF-8"));
			String str = null;
			// 计算遍历到的行号：1-270
			int count = 1;
			//存储每类矩阵，1-9个
			double[][] tmpArr = getSpeakerMatrix();
			int tagClass = 1;
			while ((str = reader.readLine()) != null) {
				String[] lineArr = str.split(" ");
				for (int j = 0; j < 13; j++) {
					// 把每行转为一个数组
					arr13[j] = Double.parseDouble(lineArr[j]);
				}
				count++;
				tmpArr[(count - 1) % 30] = arr13;
				// 把行值作为一维数组加到矩阵后，清空该数组，准备读入下一行数组。
				arr13 = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
				if (count % 30 == 0) { 
					mapSpeakers.put(tagClass, tmpArr);
					tagClass++;// 类标+1，共9类
					tmpArr = getSpeakerMatrix();// 每转换完30组，清0矩阵。
				} 
			}

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*读取test中370个sample，每个sample进行和9个类的距离计算，
	 * 然后存在数组中，然后比较得到最小的，最小所在类即是该sample所属类（发音者）
	 * **/
	public String readTxtTest(String filepath) {
		String filePath = filepath;
		BufferedReader reader = null;
		double[] arr13 = new double[] {};
		/*所有sample对应的类存储在outputContent中，格式：sampleId tagClass*/
		String outputContent = "";
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(filePath), "UTF-8"));
            /*str行的内容*/
			String str = null;
			arr13 = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };// 每块维统计(全局变量！！！！！！！！！！！！！！！！！！！！)
			while ((str = reader.readLine()) != null) {
				String[] lineArr = str.split(" ");
				/*把370个样本每个处理成一维数组*/
				for (int j = 0; j < 13; j++) {
					arr13[j] = Double.parseDouble(lineArr[j]);
				}
				int tagClass = getClassifyTag(arr13);
				/*用完arr13后要清空，及时回收，方便下一个样本使用,避免空间资源浪费*/
				arr13 = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
				outputContent += lineArr[0] + " " + tagClass + "\r\n";
				//System.out.println("sampleId:" + lineArr[0] + "---tagClass:"
				//		+ tagClass); 
			} 
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return outputContent;
	}

	/*
	 * 返回距离最近的那个类的标号：1-9之间 sample 是1-370块中的样本，是一个一维数组,长度13，第一位表示下标
	 */
	public static int getClassifyTag(double[] sample) {
		int tagClass = 0; 
		double[] distanceArr = new double[9];
		for (java.util.Map.Entry<Integer, double[][]> entry : mapSpeakers
				.entrySet()) {
			double distance = 0.0;// Math.pow(diff,2); 
			// 得到类标
			tagClass = entry.getKey();
			// 得到类标对应的矩阵
			double[][] tmp = entry.getValue();
			/*
			 * 从下标为1开始计算距离，欧几里德算法，因为下标为0表示的是类：1-9，没有具体意义 计算每维sample跟某类（1-9）距离
			 */
			for (int i = 0; i < 30; i++) {// 行的话从第0行开始，范围0-29
				for (int j = 1; j < 13; j++) {// 类的话从1开始，返回1-12 
					distance += Math.pow(tmp[i][j] - sample[j], 2);
				} 
			}

			distanceArr[tagClass - 1] = distance;// 数组下标从0开始 
		} 
	 
		// 存储最小距离，一旦新的距离比它小，则丢弃之前存储的距离，换做小距离。
		double minNum = Double.MAX_VALUE;
		int dex = 0;
		for (int j = 0; j < 9; j++) { 
			if (distanceArr[j] < minNum) {
				minNum = distanceArr[j];
				dex = j;
			}
		} 
		return dex + 1;
	}
 
}
